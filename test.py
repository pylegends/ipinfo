# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
from ipinfo import Collection, IPInfo

class TestIpInfo(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._ipinfo = IPInfo('8.8.8.8')
    def test_ipinfo(self):

        self.assertEqual(self._ipinfo.ip, '8.8.8.8')

    def test_is_dict_data(self):
        self.assertIsInstance(self._ipinfo.data, dict)

    def test_has_keys_data(self):
        for key in ['loc', 'country', 'city', 'ip', 'hostname']:
            self.assertTrue(key in self._ipinfo.data)

class TestCollection(unittest.TestCase):

    def test_collection_ips_loop_data(self):

        ips = Collection(['8.8.8.8', '4.4.4.4', '127.0.0.1'])

        for ip in ips:
            self.assertIsInstance(ip.data, dict)

    def test_is_iterable(self):

        ips = Collection(['8.8.8.8'])

        self.assertTrue(hasattr(ips, '__iter__'))

if __name__ == '__main__':
    unittest.main(verbosity=2)