# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from distutils.core import setup

setup(
    name='Testing Ipinfo',
    packages=[],
    version='0.1',
    url=None,
    download_url = 'https://wallacemaxters@bitbucket.org/pylegends/ipinfo.git',
)