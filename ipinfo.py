# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from urllib2 import urlopen
from json import loads
from socket import gethostbyname

class IPInfo(object):

    def __init__(self, ip):
        self.__ip = ip
        self._ip = ip
        self.data = loads(urlopen(self.url()).read())

    def url(self):
        return "http://ipinfo.io/%s/json" % self.__ip

    @property
    def ip(self):
        return self.__ip;

    @classmethod
    def fromhostname(cls, host):
        return cls(gethostbyname(host))

    def __repr__(self):
        return "<IPInfo %s>" % self.__ip


class Collection(object):
    def __init__(self, ips):
        if not isinstance(ips, list):
            raise Exception('The argument #2 must be list')

        self.__ips = iter(ips)

    def __iter__(self):
        return self;

    def next(self):
        return IPInfo(self.__ips.next())
